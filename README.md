 
-  **模式说明** 
- Full模式:仅接收发送数据（高性能的数据接收模型，可启用TcpKeepAlive选项）注：数据压缩丶AppKeepAlive选项在Full模式下启用无效
- Pack模式:自动处理分包（高性能的数据接收模型，自动处理分包，可启用数据压缩丶应用层心跳包丶TcpKeepAlive选项）

- **欢迎点Start后发起提问，谢谢**

```
            //使用实例

            var serverConfig = new TcpSocketSaeaServerConfiguration();
            serverConfig.KeepAlive = true;
            server = TcpSocketsFactory.CreateServerAgent(TcpSocketSaeaSessionType.Full, serverConfig, Server_CompletetionNotify);


        private void Server_CompletetionNotify(TcpSocketCompletionNotify e, TcpSocketSaeaSession session)
        {
            this.Invoke(new Action(()=> {
                switch (e)
                {
                    case TcpSocketCompletionNotify.OnConnected:

                        //session初始化完成连接的事件
                        //调用session.SendAsync();对session发送消息
                        //调用session.Close(true)断开session连接，并通知断开事件
                        //可在session.AppTokens中绑定应用对象，以便在其他异步事件中调用

                        this.listBox1.Items.Add("successful client connection");

                        break;
                    case TcpSocketCompletionNotify.OnSend:

                        //session发送数据通知事件

                        //session.SendTransferredBytes == 以发送数据长度

                        break;
                    case TcpSocketCompletionNotify.OnDataReceiveing:

                        //session数据接收通知事件

                        //session.ReceiveBytesTransferred == 本次接收数据长度
                        //Packet模式下 session.CompletedBuffer.Length == 完整数据包长度

                        this.listBox1.Items.Add("len:" + session.CompletedBuffer.Length + " msg:" + Encoding.UTF8.GetString(session.CompletedBuffer, 0, session.ReceiveBytesTransferred));
                        break;
                    case TcpSocketCompletionNotify.OnDataReceived:

                        //Packet模式session自动处理分包的完成事件

                        //var pack = session.CompletedBuffer;//完整数据包
                        break;
                    case TcpSocketCompletionNotify.OnClosed:

                        //session断开通知事件

                        this.listBox1.Items.Add("server :client offline");
                        break;
                }
            }));
        }
```